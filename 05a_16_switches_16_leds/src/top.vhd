-- Leds on = 1, off = 0, switches up = 1, down = 0

-- Exercise 1: each led turns on when its corresponding switch is up
-- Exercise 2: each led turns on when its corresponding switch is down
-- Exercise 3: all leds turn on if all switches are up
-- Exercise 4: all leds turn on if any switch is up
-- Exercise 5: each led turns on if its corresponding switch has a different value
--             to the switch on its right. The right-most led is always off  
-- Exercise 6: the leds represent an binary number which is the result of the
--             multiplication of two binary numbers. These numbers are represented
--             by the first and second groups of 8 switches
 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    sw    : in  std_logic_vector(15 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

begin

end architecture;
