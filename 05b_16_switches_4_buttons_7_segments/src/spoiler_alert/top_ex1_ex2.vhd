-- Exercise 1

-- Make a program that shows the hex representation of the 16-bit number represented
-- by the switches in the 7-segment display.
-- Because not all digits can be addressed independently, pressing the any of 4 pushbuttons
-- should select which digit to show, by activating the corresponding anode signal and driving
-- the corresponding cathodes
-- Note that both anodes and cathodes are active low (0 means digit/segment is shining)
-- To avoid rewriting code, you should use a loop, or better, build a new entity that
-- converts a 4-bit input (corresponding to the binary representation of a hex digit)
-- to a 7-bit output for the activation of the 7-segment display.
-- 
-- Hex digits representation:
--  1  2  3   4   5   6  7   8   9   A  B    C   D   E   F
--     _  _       _   _  _   _   _   _               _   _
--  |  _| _| |_| |_  |_   | |_| |_| |_| |_   _   _| |_  |_
--  | |_  _|   |  _| |_|  | |_|  _| | | |_| |_  |_| |_  |
--
-- Exercise 2
--   make sure no digit is activated if more than one button is pressed

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    sw    : in  std_logic_vector(15 downto 0);
    btn   : in  std_logic_vector(3 downto 0);
    D0_AN : out std_logic_vector(3 downto 0);
    D0_SEG: out std_logic_vector(7 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal seg_digit_0, seg_digit_1, seg_digit_2, seg_digit_3 : std_logic_vector(7 downto 0);

begin

  hexdigit7seg_0 : entity work.hexdigit7seg
  port map(hex => sw(3 downto 0), segs => seg_digit_0);
  
  hexdigit7seg_1 : entity work.hexdigit7seg
  port map(hex => sw(7 downto 4), segs => seg_digit_1);
  
  hexdigit7seg_2 : entity work.hexdigit7seg
  port map(hex => sw(11 downto 8), segs => seg_digit_2);
  
  hexdigit7seg_3 : entity work.hexdigit7seg
  port map(hex => sw(15 downto 12), segs => seg_digit_3);
  
  process(btn, seg_digit_0, seg_digit_1, seg_digit_2, seg_digit_3)
  begin

    if btn = "0001" then
      D0_AN  <= not btn;
      D0_SEG <= seg_digit_0;
    elsif btn = "0010" then
      D0_AN  <= not btn;
      D0_SEG <= seg_digit_1;
    elsif btn = "0100" then
      D0_AN  <= not btn;
      D0_SEG <= seg_digit_2;
    elsif btn = "1000" then
      D0_AN  <= not btn;
      D0_SEG <= seg_digit_3;
    else
      D0_AN  <= "1111";
      D0_SEG <= (others => '1');
    end if;
    
  end process;

  
  
end architecture;
