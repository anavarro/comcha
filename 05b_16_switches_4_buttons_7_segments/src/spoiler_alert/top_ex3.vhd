library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    sw    : in  std_logic_vector(15 downto 0);
    btn   : in  std_logic_vector(3 downto 0);
    D0_AN,D1_AN : out std_logic_vector(3 downto 0);
    D0_SEG,D1_SEG: out std_logic_vector(7 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal big_number : unsigned(31 downto 0);

  type seg_arr_t is array(7 downto 0) of std_logic_vector(7 downto 0);
  signal seg_arr : seg_arr_t;

begin

  big_number <= unsigned(sw)*unsigned(sw);
  

  gen_segs: for i in 0 to 7 generate

    hexdigit7seg_inst : entity work.hexdigit7seg
    port map(hex => std_logic_vector(big_number(i*4+3 downto i*4)), segs => seg_arr(i));
  
  end generate;
  
  process(btn, seg_arr)
  begin
    if btn = x"1" or btn = x"2" or btn = x"4" or btn = x"8" then
      D0_AN  <= not btn;
      D1_AN  <= not btn;
      
      for i in 0 to 3 loop
        if btn(i) = '1' then
          D0_SEG <= seg_arr(4+i);
          D1_SEG <= seg_arr(i);
        end if;
      end loop;
    else
      D0_AN  <= "1111";
      D1_AN  <= "1111";
      D0_SEG <= (others => '1');
      D1_SEG <= (others => '1');
    end if;
    
  end process;

  
  
end architecture;
