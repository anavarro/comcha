-- Exercise 1

-- Make a program that shows the hex representation of the 16-bit number represented
-- by the switches in the 7-segment display.
-- Because not all digits can be addressed independently, pressing the any of 4 pushbuttons
-- should select which digit to show, by activating the corresponding anode signal and driving
-- the corresponding cathodes
-- Note that both anodes and cathodes are active low (0 means digit/segment is shining)
-- To avoid rewriting code, you should use a loop, or better, build a new entity that
-- converts a 4-bit input (corresponding to the binary representation of a hex digit)
-- to a 7-bit output for the activation of the 7-segment display.
-- 
-- Hex digits representation:
--  1  2  3   4   5   6  7   8   9   A  B    C   D   E   F
--     _  _       _   _  _   _   _   _               _   _
--  |  _| _| |_| |_  |_   | |_| |_| |_| |_   _   _| |_  |_
--  | |_  _|   |  _| |_|  | |_|  _| | | |_| |_  |_| |_  |
--
-- Exercise 2
-- make sure no digit is activated if more than one button is pressed
--
-- Exercise 3
-- Same, but use both 7-segment displays. The number to be displayed is
-- the square of the number represente by the 16 switches.
-- You have to add ports to top, and modify the .xdc

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    sw    : in  std_logic_vector(15 downto 0);
    btn   : in  std_logic_vector(3 downto 0);
    D0_AN : out std_logic_vector(3 downto 0);
    D0_SEG: out std_logic_vector(7 downto 0)
  );
end entity;

architecture Behavioural of top is

begin

end architecture;
