library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity debounce is
  -- Generics are inputs to the entity that are decided on compilation time
  -- whereas ports can change during execution
  -- They can be assigned at instantiation in the "generic map" section, or
  -- left with their default value
  -- Since they are only evaluated at execution, they can have non-synthesizable types
  Generic (
    COUNTER_WIDTH : natural := 3
  );
  Port (
    clk : in  std_logic;
    i   : in  std_logic;
    o   : out std_logic
  );
end entity;

architecture Behavioral of debounce is

  signal counter    , next_counter    : unsigned(COUNTER_WIDTH-1 downto 0);
  signal stabilized , next_stabilized : std_logic;
  
  constant MAX_COUNTER : unsigned(COUNTER_WIDTH-1 downto 0) := (others => '1');
  constant MIN_COUNTER : unsigned(COUNTER_WIDTH-1 downto 0) := (others => '0');
  
begin
  
  -- o_sig is the same as o, but we want to read o's value and you cannot read
  -- from an "out" port, so you have to define a copy of it as a signal
  -- This is not the last annoying thing VHDL has...
  o <= stabilized;

  -- Sequential process: this whole thing acts as a concurrent statement
  process(clk)
  begin
    if rising_edge(clk) then
      counter     <= next_counter;
      stabilized  <= next_stabilized;
    end if;
  end process;

  -- Combinatorial concurrent statement
  next_stabilized <= counter(counter'high);

  -- Combinatorial process: this whole thing acts as a concurrent statement
  process(i, counter, stabilized, next_stabilized)
  begin
    if stabilized = '1' and next_stabilized = '0' then
      next_counter <= MIN_COUNTER;
    elsif stabilized = '0' and next_stabilized = '1' then
      next_counter <= MAX_COUNTER;
    elsif i = '1' and counter /= MAX_COUNTER then
      next_counter <= counter + 1;
    elsif i = '0' and counter /= MIN_COUNTER then
      next_counter <= counter - 1;
    else
      next_counter <= counter;
    end if;
  end process;

end architecture;
