library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity color_decoder is
  Port (
    i   : in unsigned(1 downto 0);
    o   : out std_logic_vector(2 downto 0)
  );
end entity;

architecture Behavioral of color_decoder is
begin
  
  process(i)
  begin
    o <= "000";
    if i < 3 then
      o(to_integer(i)) <= '1';
    end if;
  end process;

end architecture;
