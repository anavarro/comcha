library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ctr_incdec is
  Port (
    clk   : in  std_logic;
    incr  : in  std_logic;
    decr  : in  std_logic;
    ctr   : out unsigned
  );
end entity;

architecture Behavioral of ctr_incdec is

  signal current_ctr, next_ctr : unsigned(ctr'range);
  
begin
  
  ctr <= current_ctr;
  
  current_ctr <= next_ctr when rising_edge(clk);
  
  next_ctr <= current_ctr + 1 when incr = '1' else
              current_ctr - 1 when decr = '1' else
              current_ctr;

end architecture;
