-- Exercise
-- This is an absurdly complicated arithmetic operation. It consumes
-- all of the FPGAs DSPs and more LUTs than it has.
-- Re-implement it better, so that it fits in the FPGA.
-- Hint: this is an FPGA course, not a math contest.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    sw    : in  unsigned(7 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal sw_2  : unsigned( 15 downto 0);
  signal sw_4  : unsigned( 31 downto 0);
  signal sw_8  : unsigned( 63 downto 0);
  signal sw_16 : unsigned(127 downto 0);
  signal sw_32 : unsigned(255 downto 0);
  signal sw_64 : unsigned(511 downto 0);

begin

  sw_2  <= sw*sw;
  sw_4  <= sw_2*sw_2;
  sw_8  <= sw_4*sw_4;
  sw_16 <= sw_8*sw_8;
  sw_32 <= sw_16*sw_16;
  sw_64 <= sw_32*sw_32;
  
  process(sw_64)
  begin
    for i in led'range loop
      led(i) <= sw_64(32*i);
    end loop;
  end process;
  

  
end architecture;
