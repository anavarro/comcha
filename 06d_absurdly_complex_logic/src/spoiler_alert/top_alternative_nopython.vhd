-- Alternative solution, contributed by Pelayo Leguina from uniovi
-- Instead of python, the ROM data is generated in vhdl in the module itself
-- Not clocked, so ROM is implemented in LUTs instead of BRAM

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top_crazy is
  port(
    clk   : in  std_logic;
    sw    : in  unsigned(7 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top_crazy is

  type result_array is array(0 to 255) of std_logic_vector(511 downto 0);
  
  function result_init return result_array is
    variable temp : result_array;
    variable sw_2  : unsigned( 15 downto 0);
    variable sw_4  : unsigned( 31 downto 0);
    variable sw_8  : unsigned( 63 downto 0);
    variable sw_16 : unsigned(127 downto 0);
    variable sw_32 : unsigned(255 downto 0);
    variable sw_64 : unsigned(511 downto 0);
  begin
    forLoop: for i in 0 to 255 loop
      sw_2  := to_unsigned(i,8)*to_unsigned(i,8);
      sw_4  := sw_2*sw_2;
      sw_8  := sw_4*sw_4;
      sw_16 := sw_8*sw_8;
      sw_32 := sw_16*sw_16;
      sw_64 := sw_32*sw_32;
      temp(i) := std_logic_vector(sw_64);
      
    end loop;
  
    return temp;
  end function result_init;
  
  constant mult_results : result_array := result_init;

begin


  process(sw)
  begin
    for i in 0 to 15 loop
      led(i) <= mult_results(TO_INTEGER(sw))(32*i);
    end loop;
  end process;
  

  
end architecture;
