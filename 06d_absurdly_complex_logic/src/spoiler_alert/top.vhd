-- Exercise
-- This is an absurdly complicated arithmetic operation. It consumes
-- all of the FPGAs DSPs and more LUTs than it has.
-- Re-implement it better, so that it fits in the FPGA.
-- Hint: this is an FPGA course, not a math contest.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library work;
use work.absurd_data.all;

entity top is
  port(
    clk   : in  std_logic;
    sw    : in  unsigned(7 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is


begin

  process(clk)
  begin
    if rising_edge(clk) then
      led <= std_logic_vector(to_unsigned(rom_data(to_integer(sw)),16));
    end if;
  end process;
  

  
end architecture;
