
decimate_bits = lambda n: sum([((n >> (j*32))&1)<<j for j in range(16)])

vhdl_code = (
    """
package absurd_data is

  type rom_data_t is array(0 to 255) of integer range 2**16 -1 downto 0;
  constant rom_data : rom_data_t := ("""
    + ','.join([str(decimate_bits(pow(i,64))) for i in range(256)])
    + """);

end package;
""")

with open('absurd_data.vhd','w') as f:
    f.write(vhdl_code)
