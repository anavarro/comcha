-- Exercise:
-- This design implements a big sum that uses many LUTs, and worse, 
-- it doesn't meet timing. We actually don't care much about
-- how fast we get the result, and the inputs are pretty stationary, but
-- we are concerned that it uses too much area of the device.
-- Modify it so that it takes a picture (registers in flip-flops) its inputs,
-- then sums the 128 numbers one by one, and then outputs the result, which 
-- remains frozen while it is processing the next batch of sums 

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    btn   : in  std_logic_vector(1 downto 0);
    sw    : in  unsigned(8 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal big_vector : std_logic_vector(511 downto 0);

  type unsigned4_array_t is array(natural range <>) of unsigned(3 downto 0); 
  signal list_numbers, list_numbers_registered : unsigned4_array_t(127 downto 0);
  
  signal sum, next_sum, sum_registered: unsigned(15 downto 0);
  
  signal ctr : unsigned(7 downto 0);

begin

  big_array_inst : entity work.big_array
  port map(
    clk => clk,
    wr  => btn(1),
    addr => sw, 
    datai => btn(0),
    datao => big_vector
  );
  
  -- combinatorial process: convert the big vector into a list of small unsigneds 
  process(big_vector)
  begin
    for i in list_numbers'range loop
      list_numbers(i) <= unsigned(big_vector(i*4+3 downto i*4));
    end loop;
  end process;
  
  -- combinatorial process
  process(list_numbers_registered, ctr)
  begin
    if ctr = "0000000" then
      next_sum <= x"0000" + list_numbers_registered(0);
    else
      next_sum <= sum + list_numbers_registered(0);
    end if;
  end process;

  -- sequential process
  process(clk)
  begin
    if rising_edge(clk) then
      sum <= next_sum;

      ctr <= ctr + 1;
      
      if ctr = (ctr'range => '1') then
        list_numbers_registered <= list_numbers;
      else
        list_numbers_registered <= "0000" & list_numbers_registered(127 downto 1);
      end if;
      
      if ctr = (ctr'range => '0') then
        sum_registered <= sum;
      end if;
      
    end if;
  end process;

  led <= std_logic_vector(sum_registered);

  
end architecture;
