library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_testbench is
  -- Nothing to see here, move along
end top_testbench;

architecture Behavioral of top_testbench is

  -- You can initalize a signal with a value like this;
  -- For synthesizable signals, it's the value it'll have on power-up
  -- But in this case it's a testbench, so it's the initial value for simulation
  signal clk   : std_logic := '1';
  
  signal btn   : std_logic_vector(1 downto 0); -- this signal will be initialized later in the process
  signal led   : std_logic_vector(7 downto 0); -- these two are connected to outputs of top, so if we 
  signal RGB0  : std_logic_vector(2 downto 0); -- initialized them, they would be instantly overwritten 

  -- You can define constants like this. time type is only
  -- used for simulation
  constant CLK_PERIOD  : time := 10 ns;

begin

  top_inst : entity work.top
  port map(
    clk   => clk,
    btn   => btn,
    led   => led,
    RGB0  => RGB0
  );
  
  -- A signal that goes 1 then 0 again and again on fixed schedule...
  clk <= not clk after ( CLK_PERIOD / 2.0 );
  -- ... is called a clock
  
  process
  begin
    btn <= "00";
    
    wait for ( CLK_PERIOD );
    btn(0) <= '1';
    
    wait for ( CLK_PERIOD );
    btn(0) <= '0';
    
    wait for ( CLK_PERIOD );
    btn(0) <= '1';
    
    wait for ( CLK_PERIOD * 3 );
    btn(0) <= '0';
    
    wait for ( CLK_PERIOD * 2 );
    btn(0) <= '1';
    
    wait for ( CLK_PERIOD * 5 );
    btn(0) <= '0';
    
    wait for ( CLK_PERIOD * 2 );
    btn(0) <= '1';
    
    wait;
    
  end process;

end Behavioral;

