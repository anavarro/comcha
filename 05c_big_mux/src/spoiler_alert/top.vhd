library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    btn   : in  std_logic_vector(1 downto 0);
    sw    : in  unsigned(11 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal data : std_logic_vector(4110 downto 0);
  
  signal msb, lsb : unsigned(5 downto 0);
  signal zoom : std_logic_vector(78 downto 0);

begin

  big_array_inst : entity work.big_array
  port map(
    clk => clk,
    wr  => btn(1),
    addr => sw, 
    datai => btn(0),
    datao => data
  );

  msb <= sw(11 downto 6);
  lsb <= sw(5 downto 0);
  zoom <= data(to_integer(msb)*64 + 78 downto to_integer(msb)*64);
  led <= zoom(to_integer(lsb)+15 downto to_integer(lsb));
  
end architecture;
