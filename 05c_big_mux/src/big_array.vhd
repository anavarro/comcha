library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity big_array is
  Port (
    clk   : in  std_logic;
    wr    : in  std_logic;
    addr  : in  unsigned(11 downto 0);
    datai : in  std_logic;
    datao : out std_logic_vector(4110 downto 0)
  );
end entity;

architecture Behavioral of big_array is

  signal data_array : std_logic_vector(datao'range) := (others => '0');

begin

  process(clk)
  begin
    if rising_edge(clk) then
      if wr = '1' then
        data_array(to_integer(addr)) <= datai;
      end if;
    end if;
  end process;
  
  datao <= data_array;

end architecture;
