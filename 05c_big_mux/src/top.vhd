-- Exercise: this mux is huge; it occupies ~26% of this FPGA. That's crazy. 
-- Rewrite the mux using just combinatorial logic to make it more resource-efficient
-- Note: the big_array_inst is just our source of data, so that the sythesis doesn't
-- optimize all the logic. It allows to store bits to a big array by using the switches
-- and buttons. This module occupies 4192 LUTs (so the rest of LUT utilization is the MUX).

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    btn   : in  std_logic_vector(1 downto 0);
    sw    : in  unsigned(11 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal data : std_logic_vector(4110 downto 0);

begin

  big_array_inst : entity work.big_array
  port map(
    clk => clk,
    wr  => btn(1),
    addr => sw, 
    datai => btn(0),
    datao => data
  );

  -- LUT-devourer MUX
  led <= data(to_integer(sw)+15 downto to_integer(sw));
  
end architecture;
