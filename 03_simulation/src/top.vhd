-- Converts satisfying button pressing to shining nice led colors

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    btn   : in  std_logic_vector(1 downto 0);
    led   : out std_logic_vector(7 downto 0);
    RGB0  : out std_logic_vector(2 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal btn_debounced, btn_edges : std_logic_vector(1 downto 0);
  signal internal_counter : unsigned(led'range);
  signal color_number : unsigned(1 downto 0);

  -- This is an example of how you can keep the hierarchy only in some 
  -- parts of the design
  --attribute KEEP_HIERARCHY : string;
  --attribute KEEP_HIERARCHY of inst_ctr_incdec_for_counter: label is "TRUE";
  --attribute KEEP_HIERARCHY of inst_ctr_incdec_for_color  : label is "TRUE";

begin

  -- Loop over the two buttons, and generate, for each of them,
  -- a debouncer and an edge detector  
  gen_btns: for btn_i in 0 to 1 generate
    inst_debounce : entity work.debounce
    port map( clk => clk, i => btn(btn_i), o => btn_debounced(btn_i) );
  
    inst_edge_det : entity work.edge_detector
    port map( clk => clk, i => btn_debounced(btn_i), o => btn_edges(btn_i) );
  end generate;

  -- Count the number of edges and output that number to the array of 16 leds
  inst_ctr_incdec_for_counter : entity work.ctr_incdec
  port map( clk => clk, incr => btn_edges(0), decr => btn_edges(1), ctr => internal_counter );
  
  led <= std_logic_vector(internal_counter);
  
  -- Cycle through 4 possible colors: red, green, blue and off
  inst_ctr_incdec_for_color : entity work.ctr_incdec
  port map( clk => clk, incr => btn_edges(0), decr => btn_edges(1), ctr => color_number );
  
  inst_color_decoder : entity work.color_decoder
  port map( i => color_number, o => RGB0 );
  
end architecture;
