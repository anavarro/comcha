-- HEYYY THIS FILE CONTAINS THE SOLUTION!!
-- Do you like spoilers?

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity hexdigit7seg is
  port(
    hex  : in  std_logic_vector(3 downto 0);
    segs : out std_logic_vector(7 downto 0)
  );
end entity;

architecture Behavioural of hexdigit7seg is

begin

  segs(0) <= '0' when hex = x"0" or hex = x"2" or hex = x"3" or hex = x"5" or hex = x"6" or hex = x"7" or hex = x"8" or hex = x"9" or hex = x"A" or hex = x"E" or hex = x"F" else '1';
  segs(1) <= '0' when hex = x"0" or hex = x"1" or hex = x"2" or hex = x"3" or hex = x"4" or hex = x"7" or hex = x"8" or hex = x"9" or hex = x"A" or hex = x"D" else '1';
  segs(2) <= '0' when hex = x"0" or hex = x"1" or hex = x"3" or hex = x"4" or hex = x"5" or hex = x"6" or hex = x"7" or hex = x"8" or hex = x"9" or hex = x"A" or hex = x"B" or hex = x"D" else '1';
  segs(3) <= '0' when hex = x"0" or hex = x"2" or hex = x"3" or hex = x"5" or hex = x"6" or hex = x"8" or hex = x"9" or hex = x"B" or hex = x"C" or hex = x"D" or hex = x"E" else '1';
  segs(4) <= '0' when hex = x"0" or hex = x"2" or hex = x"6" or hex = x"8" or hex = x"A" or hex = x"B" or hex = x"C" or hex = x"D" or hex = x"E" or hex = x"F" else '1';
  segs(5) <= '0' when hex = x"0" or hex = x"4" or hex = x"5" or hex = x"6" or hex = x"8" or hex = x"9" or hex = x"A" or hex = x"B" or hex = x"E" or hex = x"F" else '1';
  segs(6) <= '0' when hex = x"2" or hex = x"3" or hex = x"4" or hex = x"5" or hex = x"6" or hex = x"8" or hex = x"9" or hex = x"A" or hex = x"B" or hex = x"C" or hex = x"D" or hex = x"E" or hex = x"F" else '1';
  segs(7) <= '1';
  
end architecture;
