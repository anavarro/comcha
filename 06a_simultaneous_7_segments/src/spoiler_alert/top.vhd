library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    sw    : in  std_logic_vector(15 downto 0);
    D0_AN,D1_AN : out std_logic_vector(3 downto 0);
    D0_SEG,D1_SEG: out std_logic_vector(7 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal big_number : unsigned(31 downto 0);

  type seg_arr_t is array(7 downto 0) of std_logic_vector(7 downto 0);
  signal seg_arr : seg_arr_t;
  
  signal ctr : unsigned(19 downto 0);

begin

  big_number <= unsigned(sw)*unsigned(sw);

  gen_segs: for i in 0 to 7 generate

    hexdigit7seg_inst : entity work.hexdigit7seg
    port map(hex => std_logic_vector(big_number(i*4+3 downto i*4)), segs => seg_arr(i));
  
  end generate;
  
  ctr <= ctr + 1 when rising_edge(clk);

  process(ctr, seg_arr)
  begin
    for i in 0 to 3 loop
      if i = to_integer(ctr(ctr'high downto ctr'high-1)) then
        D0_AN(i)  <= '0';
        D1_AN(i)  <= '0';
      else
        D0_AN(i)  <= '1';
        D1_AN(i)  <= '1';
      end if;
    end loop;
    
    D0_SEG <= seg_arr(to_integer(ctr(ctr'high downto ctr'high-1))+4);
    D1_SEG <= seg_arr(to_integer(ctr(ctr'high downto ctr'high-1))  );
    
  end process;

end architecture;
