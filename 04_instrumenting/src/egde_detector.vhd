library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity edge_detector is
  Port (
    clk : in  std_logic;
    i   : in  std_logic;
    o   : out std_logic);
end entity;

architecture Behavioral of edge_detector is

  signal i_last : std_logic;

begin

  i_last <= i when rising_edge(clk);

  o <= i and not i_last;

end architecture;


