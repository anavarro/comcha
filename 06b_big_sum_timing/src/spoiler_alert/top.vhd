-- Exercise: This design does not meet timing. There is a huge sum of 128
-- numbers that is done directly in one step (one clock cycle).
-- Modify the sum to create an intermediate result with partial sums,
-- for example do 2 sums of 64 numbers, and then, in a second clock cycle,
-- sum those 2 partial results into the final number

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    btn   : in  std_logic_vector(1 downto 0);
    sw    : in  unsigned(8 downto 0);
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal big_vector : std_logic_vector(511 downto 0);

  type unsigned4_array_t is array(natural range <>) of unsigned(3 downto 0); 
  signal list_numbers : unsigned4_array_t(127 downto 0);
  
  signal sum, next_sum, partial1, partial2, next_partial1, next_partial2 : unsigned(15 downto 0);

begin

  big_array_inst : entity work.big_array
  port map(
    clk => clk,
    wr  => btn(1),
    addr => sw, 
    datai => btn(0),
    datao => big_vector
  );
  
  -- combinatorial process: convert the big vector into a list of small unsigneds 
  process(big_vector)
  begin
    for i in list_numbers'range loop
      list_numbers(i) <= unsigned(big_vector(i*4+3 downto i*4));
    end loop;
  end process;
  
  -- combinatorial process
  process(list_numbers, partial1, partial2)
    variable temp_sum : unsigned(15 downto 0);
  begin
    temp_sum := (others => '0');
    for i in 0 to 63 loop
      temp_sum := temp_sum + list_numbers(i);
    end loop;
    next_partial1 <= temp_sum;
    
    temp_sum := (others => '0');
    for i in 64 to 127 loop
      temp_sum := temp_sum + list_numbers(i);
    end loop;
    next_partial2 <= temp_sum;
    
    next_sum <= partial1 + partial2;
    
  end process;

  -- sequential process
  process(clk)
  begin
    if rising_edge(clk) then
      partial1 <= next_partial1;
      partial2 <= next_partial2;
      sum <= next_sum;
    end if;
  end process;

  led <= std_logic_vector(sum);
  
end architecture;
