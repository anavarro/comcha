-- This is a comment
-- People usually put their name here, so...
-- Alvaro

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity top is
  port(
    clk   : in  std_logic;
    led   : out std_logic_vector(15 downto 0)
  );
end entity;

architecture Behavioural of top is

  signal counter : unsigned(36 downto 0);

begin
  
  counter <= counter + 1 when rising_edge(clk);
  led <= std_logic_vector( counter(counter'high downto counter'high - led'high) );
  
end architecture;
